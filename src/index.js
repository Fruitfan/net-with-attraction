import {Graphics, Application, Text} from "pixi.js";


class NetNode extends Graphics {
    constructor (x, y) {
        super();

        this.id = NetNode.count;
        NetNode.count++;

        this.x = x;
        this.y = y;
        this.originalPosition = {
            x: x,
            y: y
        };

        this.size = Math.floor(Math.random() * 25);
        this.radius = this.size + 10;
        this.range = this.radius * 7;

        this.maxConnections = 2;

        this.connections = [];
        this.distances = {};

        this.displacement = {
            x: 0,
            y: 0
        };

        this.setCircleColor();
        this.drawCircle(0,0, this.radius);
        this.endFill();


        // show node ID
        //var idText = new Text(this.id);
        //idText.x = -10;
        //idText.y = -10;

        //this.addChild(idText);
    }

    setCircleColor () {
        this.beginFill(0xaeafb0, 1 / ((this.size / 2) + 1));
    }

    /**
     * returns the distance to another node.
     * @param {NetNode} node
     */
    getDistanceTo (node) {
        if (node.id != null && this.distances[node.id] != null) return this.distances[node.id];

        let distanceX = Math.abs(node.x - this.x);
        let distanceY = Math.abs(node.y - this.y);
        let distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

        this.distances[node.id] = distance;

        return distance;
    }

    connectTo (node) {
        if (node == null) {
            console.error('connecting to null', this.id, this.connections);
            return;
        }
        if (this.connections.indexOf(node.id) !== -1) return;

        if (this.connections.length >= this.maxConnections) return;
        if (node.connections.length >= node.maxConnections) return;

        this.connections.push (node.id);
        node.connections.push (this.id);

        return {
            a: this,
            b: node
        };
    }

    findClosestNode (nodes, nodesToExclude = [], onlyAvailable = false) {
        //console.log ('excluding nodes', nodesToExclude);

        let distance = Infinity;
        let closestNode = null;
        nodes.forEach ((node) => {
            if (node.id === this.id) return;
            if (nodesToExclude.indexOf(node.id) !== -1) return;

            if (onlyAvailable === true && !node.isOpenForConnections) return;

            let nodeDistance = this.getDistanceTo(node);
            if (nodeDistance < distance) {
                distance = nodeDistance;
                closestNode = node;
            }
        });
        return closestNode;
    }

    connectToClosestAvailable (nodes) {
        let closestUnattachedNode = this.findClosestNode(nodes, this.connections, true);
        if (closestUnattachedNode === null) return null;
        return this.connectTo (closestUnattachedNode);
    }

    get isOpenForConnections () {
        return (this.connections.length < this.maxConnections);
    }

    reactToMouse(mousePos) {
        let range = this.range;
        // only react if in range rectange
        if (mousePos.x < this.x - range || mousePos.x > this.x + range || mousePos.y < this.y - range || mousePos.y > this.y + range) {
            this.scale.y = 1;
            this.scale.x = 1;
            return;
        }

        let closeness = (range - this.getDistanceTo(mousePos));
        //console.log (closeness, range, mousePos, this.x, this.y);
        if (closeness < 0) closeness = 0;
        closeness = closeness / range;

        this.scale.x = 1 + closeness;
        this.scale.y = 1 + closeness;

        this.x = this.originalPosition.x + (mousePos.x - this.x) * closeness;
        this.y = this.originalPosition.y + (mousePos.y - this.y) * closeness;
    }
}

NetNode.count = 0;

class AttractiveNet {

    /**
     *
     * @param {HTMLElement} element
     * @param {Object} options
     */
    constructor (element, options = {}) {

        // options:
        // * nodecount
        // * class
        // * id

        this.element = element;
        this.nodecount = (options.nodecount === undefined) ? 140 : options.nodecount;

        this.width = element.offsetWidth;
        this.height = element.offsetHeight;

        let app = new Application({
            width: this.width,
            height: this.height,
            backgroundColor: 0xffffff,
            autoResize: true,
            resolution: devicePixelRatio * 2
        });
        this.app = app;

        // add class and id
        app.view.className = (options.class === undefined) ? "net-attractive" : options.class;
        app.view.id = (options.id === undefined) ? "" : options.id;

        this.graphics = new Graphics ();
        this.lines = new Graphics();
        this.graphics.addChild(this.lines);
        app.stage.addChild(this.graphics);

        this.addMouseEvents(app);


        this.nodes = [];
        this.connections = [];

        this.createNodes();

        element.appendChild(app.view);

        window.addEventListener('resize', this.resize.bind(this));
    }

    resize () {
        this.width = this.element.offsetWidth;
        this.height = this.element.offsetHeight;
        this.app.renderer.resize(this.width, this.height);
    }

    createNodes () {
        for (let i = 0; i < this.nodecount; i++) {
            this.createNode();
        }

        this.connectNodes(1);
        this.setNodeMaxConnections(7);
        this.connectNodes(1);
        this.connectNodes(1);
        this.connectNodes(1);
        this.connectNodes(1);

        this.drawConnections();
    }

    /**
     *
     * @param {Application} app
     */
    addMouseEvents (app) {
        app.stage.interactive = true;
        app.stage.on('pointermove', (event) => {
            this.update(event);
        });
    }
    update(event) {
        // mouse position:
        let mousePos = event.data.global;
        //console.log (mousePos);
        this.nodes.forEach((node) => {
            node.reactToMouse(mousePos);
        });
        this.drawConnections();
    }

    setNodeMaxConnections (max) {
        this.nodes.forEach((node) => {
            node.maxConnections = max;
        });
    }

    createNode () {
        let offsetX = 50; // 50
        let offsetY = 50; // 50
        let node = new NetNode(Math.random() * (this.width + offsetX*2) - offsetX, Math.random() * (this.height + offsetY*2) - offsetY);
        this.graphics.addChild(node);
        this.nodes.push(node);
    }

    connectNodes (strength) {
        let availableNodes = this.getAvailableNodes();

        let stopped = false;
        availableNodes.forEach((node) => {

            if (stopped === true) return;

            //let closestNode = node.findClosestNode(this.nodes);
            //let connection = node.connectTo(closestNode);

            let connection = node.connectToClosestAvailable(this.nodes);

            // stop trying to connect if no connections are available anymore.
            if (connection === null) {
                stopped = true;
                return;
            }

            if (connection != null) this.connections.push({connection: connection, strength: strength, ids: connection.a.id + '-' + connection.b.id  });

            //else console.log ('no connection for', node.id);

            //this.drawConnection(connection, strength);
        });
    }

    getAvailableNodes () {
        return this.nodes.filter((node) => {
            if (!node.isOpenForConnections) return false;
            return true;
        });
    }

    drawConnection (connection, strength) {
        if (connection == null) {
            console.error ('No connection.');
            return;
        }
        let {a, b} = connection;

        this.lines.lineStyle(strength, 0xaeafb0, (strength * 50) / 100);
        this.lines.moveTo(a.x, a.y);
        this.lines.lineTo(b.x, b.y);
        //console.log (connection);
    }

    drawConnections () {
        //console.table (this.connections.map((entry) => entry.ids));
        this.lines.clear();
        this.connections.forEach((entry) => {
            this.drawConnection(entry.connection, entry.strength);
        });
    }
}

window.AttractiveNet = AttractiveNet;

export default AttractiveNet;